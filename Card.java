public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	public String getSuit(){
		return this.suit;
	}	
	public String getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " of " +this.suit;
	}	
	public double calculateScore(){
		double score = 0.0;
		
		double firstvalue = Double.parseDouble(this.value);
		score += firstvalue;
		if(this.suit.equals("Heart")){
			score += 0.4;
		}	
		if(this.suit.equals("Diamond")){
			score += 0.3;
		}	
		if(this.suit.equals("Club")){
			score += 0.2;
		}	
		if(this.suit.equals("Spade")){
			score += 0.1;
		}	
		
		return score;
	}	
}	