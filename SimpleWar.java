public class SimpleWar{
	public static void main(String[] args){
		Deck myDeck = new Deck();
		myDeck.shuffle();
		System.out.println(myDeck);
		
		int player1 = 0;
		int player2 = 0;
		
		
		
		
		while(myDeck.length() > 1){
			
			Card first1 = myDeck.drawTopCard();
			System.out.println(first1);
			System.out.println(first1.calculateScore());
			Card first2 = myDeck.drawTopCard();
			System.out.println(first2);
			System.out.println(first2.calculateScore());
		
			System.out.println("Points of Player 1: " + player1);
			System.out.println("Points of Player 2: " + player2);
			
			if(first1.calculateScore() > first2.calculateScore()){
				System.out.println("The player 1 won with: " + first1);
				player1 += 1;
			}
			else{
				System.out.println("The player 2 won with: " + first2);
				player2 += 1;
			}
			System.out.println("Points of Player 1: " + player1);
			System.out.println("Points of Player 2: " + player2);	
		}
		
		if(player1 > player2){
			System.out.println("Congratulation! The player 1 won!");
		}	
		else if(player1 < player2){
			System.out.println("Congratulation! The player 2 won!");
		}
		else{
			System.out.println("It's a tie");
		}	
	}	
}	